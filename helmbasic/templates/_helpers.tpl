{{/* Common Labels */}}
{{- define "helmbasic.labels" }}
    app: nginx
    chartname: {{ .Chart.Name }}
    template-in-template: {{ include "helmbasic.resourceName" . }}
{{- end }}

{{/* k8s resources name: string concat with hyphen */}}
{{- define "helmbasic.resourceName" }}
{{- printf "%s-%s" .Release.Name .Chart.Name }}
{{- end }}
